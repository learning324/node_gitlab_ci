import server from "../main"; //import file we are testing

describe("testing-server-routes", () => {
  it("GET /ping", async () => {
    const res = await server.inject({
      url: "/ping",
    });
    expect(res.statusCode).toEqual(200);
  });
});
