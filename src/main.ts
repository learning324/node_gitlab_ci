import Fastify, { fastify, FastifyInstance, RouteShorthandOptions } from 'fastify'
import { Server, IncomingMessage, ServerResponse } from 'http'
import * as  Sentry from "@sentry/node"
import Tracing from "@sentry/tracing";

require('newrelic');

const server: FastifyInstance = Fastify({
  logger: true,
  pluginTimeout: 1000000,
  trustProxy: true,
})

Sentry.init({
  dsn: "https://17e7cb607e63471e8996049fece86e70@o1170103.ingest.sentry.io/6463386",
  tracesSampleRate: 1.0,
  environment: "production",
  release: `0.0.0`,
  debug: true,
  integrations: [ new Sentry.Integrations.Http({ tracing: true }) ],
});




const opts: RouteShorthandOptions = {
  schema: {
    response: {
      200: {
        type: 'object',
        properties: {
          pong: {
            type: 'string'
          }
        }
      }
    }
  }
}

server.get('/ping', opts, async (request, reply) => {
  try {
    const transaction = Sentry.startTransaction({
      op: "test",
      name: "My First Test Transaction",
    });
    setTimeout(() => {
      try {
        // foo();
      } catch (e) {
        Sentry.captureException(e);
      } finally {
        transaction.finish();
      }
    }, 99);
    
  } catch (error) {
    Sentry.captureException(error);

  }
  return { pong: 'it worked!' }
})






 



const start = async () => {
  try {
    await server.listen(3000)

    const address = server.server.address()
    const port = typeof address === 'string' ? address : address?.port


    server.log.info(`Server listening on port ${address}`)
  } catch (err) {
    server.log.error(err)
    Sentry.captureException(err);
  }
}
start()

export default server;